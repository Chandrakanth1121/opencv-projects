import numpy as np
import cv2
import matplotlib.pyplot as plt
import webcolors
img=cv2.imread('building.jpg',1)
cv2.imshow('image',img)
img=cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
img=img.reshape(img.shape[0]*img.shape[1],3)
clusters=5
def closest_colour(requested_colour):
    min_colours = {}
    for key, name in webcolors.css3_hex_to_names.items():
        r_c, g_c, b_c = webcolors.hex_to_rgb(key)
        rd = (r_c - requested_colour[0]) ** 2
        gd = (g_c - requested_colour[1]) ** 2
        bd = (b_c - requested_colour[2]) ** 2
        min_colours[(rd + gd + bd)] = name
    return min_colours[min(min_colours.keys())]

def get_colour_name(requested_colour):
    try:
        closest_name = actual_name = webcolors.rgb_to_name(requested_colour)
    except ValueError:
        closest_name = closest_colour(requested_colour)
        actual_name = None
    return actual_name, closest_name

from sklearn.cluster import KMeans
kmeans=KMeans(clusters)
kmeans.fit(img)
colors=kmeans.cluster_centers_
labels=kmeans.labels_
pixel_count=0
label_count=[0 for i in range(clusters)]
for ele in labels:
    label_count[ele] +=1
o=0
pixels=np.array([0,0,0,0,0])
for i in range (clusters):
    pixels[o]=label_count[i]
    o+=1
biggest=0
for i in range (clusters):
    if pixels[i]>biggest:
        biggest=pixels[i]
        pos1=i
#print (biggest)
secondbiggest=0
for i in range (clusters):
    if pixels[i]>secondbiggest and pixels[i]<biggest:
        secondbiggest=pixels[i]
        pos2=i
#print (secondbiggest)
print ('most dominant color: ')
actual_name, closest_name = get_colour_name(colors[pos1])
print ("Actual colour name:", actual_name, ", closest colour name:", closest_name)
cv2.waitKey(0)
cv2.destroyAllWindows()