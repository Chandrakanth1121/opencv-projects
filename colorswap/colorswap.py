import numpy as np
import cv2
import matplotlib.pyplot as plt
import webcolors
img=cv2.imread('opencv.jpeg',1)
imgafter=img
cv2.imshow('before swap',img)
img=cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
img=img.reshape(img.shape[0]*img.shape[1],3)
clusters=15
def closest_colour(requested_colour):
    min_colours = {}
    for key, name in webcolors.css3_hex_to_names.items():
        r_c, g_c, b_c = webcolors.hex_to_rgb(key)
        rd = (r_c - requested_colour[0]) ** 2
        gd = (g_c - requested_colour[1]) ** 2
        bd = (b_c - requested_colour[2]) ** 2
        min_colours[(rd + gd + bd)] = name
    return min_colours[min(min_colours.keys())]

def get_colour_name(requested_colour):
    try:
        closest_name = actual_name = webcolors.rgb_to_name(requested_colour)
    except ValueError:
        closest_name = closest_colour(requested_colour)
        actual_name = None
    return actual_name, closest_name

from sklearn.cluster import KMeans
kmeans=KMeans(clusters)
kmeans.fit(img)
colors=kmeans.cluster_centers_
labels=kmeans.labels_
pixel_count=0
label_count=[0 for i in range(clusters)]
for ele in labels:
    label_count[ele] +=1
o=0
pixels= np.zeros((15),int)
for i in range (clusters):
    pixels[o]=label_count[i]
    o+=1
biggest=0
for i in range (clusters):
    if pixels[i]>biggest:
        biggest=pixels[i]
        pos1=i
#print (biggest)
secondbiggest=0
for i in range (clusters):
    if pixels[i]>secondbiggest and pixels[i]<biggest:
        secondbiggest=pixels[i]
        pos2=i
#print (secondbiggest)
print ('most dominant color: ')
actual_name, closest_name = get_colour_name(colors[pos1])
print ("Actual colour name:", actual_name, ", closest colour name:", closest_name)
print ('second most dominant color: ')
actual_name1, closest_name1 = get_colour_name(colors[pos2])
print ("Actual colour name:", actual_name1, ", closest colour name:", closest_name1)
r=round(colors[pos1][0])
g=round(colors[pos1][1])
b=round(colors[pos1][2])
r1=round(colors[pos2][0])
g1=round(colors[pos2][1])
b1=round(colors[pos2][2])
#r,g,b=webcolors.name_to_rgb(closest_name)
#r1,g1,b1=webcolors.name_to_rgb(closest_name1)
print(r,g,b)
print(r1,g1,b1)
for i in range (imgafter.shape[0]):
    for j in range (imgafter.shape[1]):
        if imgafter[i][j][0]==b and imgafter[i][j][1]==g and imgafter[i][j][2]==r:
            imgafter[i][j][0] = b1
            imgafter[i][j][1] = g1
            imgafter[i][j][2] = r1
        elif imgafter[i][j][0]==b1 and imgafter[i][j][1]==g1 and imgafter[i][j][2]==r1:
            imgafter[i][j][0] = b
            imgafter[i][j][1] = g
            imgafter[i][j][2] = r
        else:
            continue
cv2.imshow('after swap',imgafter)
cv2.waitKey(0)
cv2.destroyAllWindows()