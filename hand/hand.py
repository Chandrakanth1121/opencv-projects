import cv2
import numpy as np

cap = cv2.VideoCapture(0)
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('output.avi', fourcc, 20.0, (640, 480))
while (cap.isOpened()):
    ret, frame= cap.read()
    frame=cv2.flip(frame,1)
    roi=frame[0:400,0:300]
    cv2.rectangle(frame,(0,0),(300,400),(225,225,225),3)

    if ret == True:
        gray=cv2.cvtColor(roi,cv2.COLOR_BGR2GRAY)
        fr = cv2.GaussianBlur(gray, (81,81),cv2.BORDER_DEFAULT)
        ret,thresh=cv2.threshold(fr,150,255,cv2.THRESH_BINARY_INV)
        contours,hierarchy=cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
        if(len(contours)==0):
            continue
        c = max(contours, key=cv2.contourArea)
        hull=[cv2.convexHull(c)]
        areahull=cv2.contourArea(hull[0])
        areacontour=cv2.contourArea(c)
        r=((areahull-areacontour)/areacontour)*100
        print(r)
        cv2.drawContours(frame,hull,-1,(0,255,0),3)
        cv2.imshow('thresh',thresh)
        font=cv2.FONT_HERSHEY_COMPLEX
        if r>25:
            cv2.putText(frame,'open hand',(5,50),font,1,(0,0,255),2)
        #elif r>47 and r<51:
        #    cv2.putText(frame, '2', (5, 50), font, 1, (0, 0, 255), 2)
        else:
            cv2.putText(frame, 'fist', (5, 50), font, 1, (0, 0, 255), 2)
        out.write(frame)
        cv2.imshow('video', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break

cap.release()
out.release()
cv2.destroyAllWindows()
